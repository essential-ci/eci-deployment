set -e

helm repo add bitnami https://charts.bitnami.com/bitnami
helm upgrade --install kafka bitnami/kafka -f kafka/values.yaml -n eci --create-namespace
helm upgrade --install mongodb bitnami/mongodb -f mongodb/values.yaml -n eci --create-namespace
helm repo add jetstack https://charts.jetstack.io
helm upgrade --install cert-manager jetstack/cert-manager -f cert-manager/values.yaml -n cert-manager --create-namespace
