helm delete kafka -n eci
helm delete mongodb -n eci
helm delete cert-manager -n cert-manager
sleep 5
kubectl delete pvc -l app.kubernetes.io/name=kafka -n eci
kubectl delete pvc -l app.kubernetes.io/name=zookeeper -n eci
kubectl delete pvc -l app.kubernetes.io/name=mongodb -n eci
